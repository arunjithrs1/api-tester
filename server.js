var express = require('express');
var mongoose = require('mongoose'); 
var bodyParser = require('body-parser');
var Release = require('./model');

// database connection
mongoose.connect('mongodb+srv://aaster:aaster@cluster0-jhzkp.mongodb.net/nest-demo?retryWrites=true&w=majority'); 

// On connection
mongoose.connection.on('connected', function(){
  console.log('Connected to database ');
});


var app = express(); 	

app.use(bodyParser.urlencoded({'extended': 'true'}));

app.use(bodyParser.json());

app.use(express.static('./public')); 


app.post('/api/lat', function(req, res) {

	let newRelease = new Release({
    	lant: req.body.lat
  	});

	newRelease.save(function(err){
		if (err) throw err;
		res.json({success: true, message: "New latlont added"});
	});

});



var port = process.env.PORT || 3000; 	

app.listen(port);

console.log("App listening on port " + port);