var mongoose = require('mongoose');

const releaseSchema = new mongoose.Schema({
    latlont: {
        type: String,
    },
});

const Release = module.exports = mongoose.model('latlont', releaseSchema);